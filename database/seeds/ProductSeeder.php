<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('products')->insert([
            [
                'id' => 1,
                'category_id' => 6,
                'price' => 31.30,
                'name' => 'Rica Шампунь для разглаживания и выпрямления волос',
                'description' => 'Rica Шампунь для разглаживания и выпрямления волос description',
            ],
            [
                'id' => 2,
                'category_id' => 6,
                'price' => 32.30,
                'name' => 'Rica Шампунь для разглаживания и выпрямления волос2',
                'description' => 'Rica Шампунь для разглаживания и выпрямления волос description2',
            ],
        ]);
    }
}
