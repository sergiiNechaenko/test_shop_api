<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('product_categories')->insert([
            [
                'id' => 1,
                'parent_category_id' => null,
                'name' => 'Волосы',
            ],
            [
                'id' => 2,
                'parent_category_id' => null,
                'name' => 'Лицо',
            ],
            [
                'id' => 3,
                'parent_category_id' => null,
                'name' => 'Тело',
            ],
            [
                'id' => 4,
                'parent_category_id' => null,
                'name' => 'Макияж',
            ],
            [
                'id' => 5,
                'parent_category_id' => null,
                'name' => 'Ногти',
            ],
            [
                'id' => 6,
                'parent_category_id' => 1,
                'name' => 'Уход за волосами',
            ],
            [
                'id' => 7,
                'parent_category_id' => 1,
                'name' => 'Наращивание волос',
            ],
        ]);
    }
}
