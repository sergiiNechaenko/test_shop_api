<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [\App\Http\Controllers\ApiControllers\AuthController::class, 'register']);
Route::post('/login', [\App\Http\Controllers\ApiControllers\AuthController::class, 'login']);

Route::middleware(['auth:api'])->group(function () {
    Route::get('/categories', [\App\Http\Controllers\ApiControllers\CategoryController::class, 'get']);
    Route::get('/products', [\App\Http\Controllers\ApiControllers\ProductController::class, 'get']);
});
