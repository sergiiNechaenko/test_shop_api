<?php


namespace App\Http\Controllers\ApiControllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

/**
 * @group Auth methods
 */
class AuthController extends Controller
{
    /**
     * Registration
     *
     * @bodyParam  user_type string required The type of the user['client','seller,'company']. Example: client
     * @bodyParam  email email required User`s email. Example: test@test.test
     * @bodyParam  password string required User`s password. Example: testPassword
     * @bodyParam  password_confirmation string required User`s password password_confirmation. Example: testPassword
     *
     * @response {
     *    "user_type": "client",
     *    "email": "test@test.test",
     *    "api_token": "L1Y0f369ozrGt12z34iUvXe40NRckzSePDGrmbWcyZO0Q4h5TEtySefuZ7Am",
     *    "updated_at": "2020-09-21T10:18:04.000000Z",
     *    "created_at": "2020-09-21T10:18:04.000000Z",
     *    "id": 2
     * }
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_type' => ['required', 'string', 'in:client,seller,company'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $user = User::create([
            'user_type' => $request->user_type,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'api_token' => Str::random(60),
        ]);

        return response()->json($user, 200);
    }
    /**
     * Login
     *
     * @bodyParam  email email required User`s email. Example: test@test.test
     * @bodyParam  password string required User`s password. Example: testPassword
     *
     * @response {
     *    "user_type": "client",
     *    "email": "test@test.test",
     *    "api_token": "L1Y0f369ozrGt12z34iUvXe40NRckzSePDGrmbWcyZO0Q4h5TEtySefuZ7Am",
     *    "updated_at": "2020-09-21T10:18:04.000000Z",
     *    "created_at": "2020-09-21T10:18:04.000000Z",
     *    "id": 2
     * }
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255', 'exists:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 401);
        }
        $user = User::where('email', $request->email)->first();
        if (!Hash::check($request->password, $user->password)) {
            return response()->json(['Wrong password'], 401);
        }
        return $user;
    }
}
