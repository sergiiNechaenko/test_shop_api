<?php


namespace App\Http\Controllers\ApiControllers;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

/**
 * @group Products methods
 */
class CategoryController extends Controller
{
    /**
     * Fetch categories
     *
     *
     * Fetch categories by parent category
     *
     * @queryParam  api_token required The user`s auth token. Example: Ln7sIodcJRHUM6hyROPc2RaQSD1qkr717rua1pwDsN24zwDDnafxlHiIkoBU
     * @queryParam  parent_category required The parent category ID. Example: 0
     *
     * @response {
     *  "data": [
     *   {
     *    "id": 1,
     *    "parent_category_id": null,
     *    "name": "Волосы",
     *    "updated_at": "2020-09-21T10:18:04.000000Z",
     *    "created_at": "2020-09-21T10:18:04.000000Z"
     *   },
     *   {
     *    "id": 6,
     *    "parent_category_id": 1,
     *    "name": "Уход за волосами",
     *    "updated_at": "2020-09-21T10:18:04.000000Z",
     *    "created_at": "2020-09-21T10:18:04.000000Z"
     *   }
     *  ]
     * }
     */
    public function get()
    {
        $validator = Validator::make(request()->all(), [
            'parent_category' => ['required', 'int'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $parentCategoryId = (int)request()->parent_category;
        if (0 === $parentCategoryId) { // main categories
            return response()->json(['data' => Category::whereNull('parent_category_id')->get()], 200);
        }
        $parentCategory = Category::find($parentCategoryId);
        return response()->json(['data' => $parentCategory->childCategories], 200);
    }
}
