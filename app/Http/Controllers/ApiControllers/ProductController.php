<?php


namespace App\Http\Controllers\ApiControllers;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

/**
 * @group Products methods
 */
class ProductController extends Controller
{
    /**
     * Fetch products
     *
     *
     * Fetch products by category
     *
     * @queryParam  api_token required The user`s auth token. Example: Ln7sIodcJRHUM6hyROPc2RaQSD1qkr717rua1pwDsN24zwDDnafxlHiIkoBU
     * @queryParam  category required The category ID. Example: 6
     *
     * @response {
     *  "data": [
     *   {
     *    "id": 1,
     *    "category_id": 6,
     *    "name": "Rica Шампунь для разглаживания и выпрямления волос",
     *    "description": "Rica Шампунь для разглаживания и выпрямления волос description",
     *    "price": 31.3,
     *    "updated_at": "2020-09-21T10:18:04.000000Z",
     *    "created_at": "2020-09-21T10:18:04.000000Z"
     *   }
     *  ]
     * }
     */
    public function get()
    {
        $validator = Validator::make(request()->all(), [
            'category' => ['required', 'int', 'exists:product_categories,id'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $category = Category::find(request()->category);
        return response()->json(['data' => $category->products], 200);
    }
}
